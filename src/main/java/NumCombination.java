import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NumCombination {
    private List<Integer> nums;


    public NumCombination(int num1, int num2, int num3) {
        nums = new ArrayList<>();
        nums.add(num1);
        nums.add(num2);
        nums.add(num3);
        Collections.sort(nums);
    }

    public List<Integer> getNums() {
        return nums;
    }

    public void setNums(List<Integer> nums) {
        this.nums = nums;
    }

    @Override
    public String toString() {
        return "(" + nums.get(0) +
                "," + nums.get(1) +
                "," + nums.get(2) +
                ')';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj instanceof NumCombination) {
            NumCombination target = (NumCombination) obj;
            for (int i = 0; i < target.getNums().size(); i++) {
                if (target.getNums().get(i) != this.nums.get(i)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
