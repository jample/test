

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("请输入1到24个数（用英文的逗号隔开，回车结束）：");
        Scanner scanner = new Scanner(System.in);
        String allNums = scanner.nextLine();
        System.out.println("输入excel路径如C:\\\\文件夹\\excel名.xls或xlsx(回车结束)：");
        String readExcelPath = scanner.nextLine().trim();
        System.out.println("输入要输出的文件路径如C:\\\\文件夹\\名字.txt(回车结束)：");
        String outPutFilePath = scanner.nextLine().trim();
        File file = new File(outPutFilePath);
        FileOutputStream outputStream = new FileOutputStream(file);
        String[] allNumsStr = allNums.split(",");
        List<Integer> nums = Arrays.stream(allNumsStr).map(numStr -> Integer.parseInt(numStr)).collect(Collectors.toList());
        Collections.sort(nums);
        List<NumCombination> numCombinations = createNumCombination(nums);
        output(outputStream, "输入排列组合：\r\n");
        System.out.println("输入排列组合：");

        //没什么东西
        numCombinations.forEach(numCombination -> {
            output(outputStream, numCombination.toString() + "\r\n");
            System.out.println(numCombination.toString());
        });
        output(outputStream,"总大小：" + numCombinations.size()+"\r\n");
        System.out.println("总大小：" + numCombinations.size());
        List<List<Integer>> excelAllData = ExcelUtil.readExcel(new File(readExcelPath));
        output(outputStream, "excel 总行数：" + excelAllData.size() + "\r\n");

        System.out.println("excel 总行数：" + excelAllData.size());
        int line = 1;
        for (List<Integer> excelLineData : excelAllData) {
            Set<NumCombination> differenceSet = new HashSet<>();
            List<NumCombination> excelOneLineNums = createNumCombination(excelLineData);
            output(outputStream, "\r\n");
            System.out.println();
            output(outputStream, "计算第" + line + "行excel" + "\r\n");
            System.out.println("计算第" + line + "行excel");
            output(outputStream, "==============================================================="  + "\r\n");
            System.out.println("===============================================================");
            int theSameAmount = compareTwoNums(numCombinations, excelOneLineNums, differenceSet, outputStream);
            output(outputStream, "第" + line + "行相同数为：" + theSameAmount + "\r\n");
            System.out.println("第" + line + "行相同数为：" + theSameAmount);
            output(outputStream, "===============================================================" + "\r\n");
            System.out.println("===============================================================");
            Set<Integer> differenceNumSet = differenceSet.stream().map(NumCombination::getNums).flatMap(numCombinationNums -> numCombinationNums.stream()).collect(Collectors.toSet());
            String differenceStr = differenceNumSet.stream().map(n -> n + "").collect(Collectors.joining(","));
            output(outputStream, "最终数据：" + differenceStr + "\r\n");
            System.out.println("最终数据：" + differenceStr);
            line++;
        }
        outputStream.close();


    }

    private static int compareTwoNums(List<NumCombination> numCombinations, List<NumCombination> excelOneLineNums, Set<NumCombination> differenceSet, FileOutputStream outputStream) {
        int theSameAmount = 0;
        for (int i = 0; i < numCombinations.size(); i++) {
            for (int j = 0; j < excelOneLineNums.size(); j++) {
                if (numCombinations.get(i).equals(excelOneLineNums.get(j))) {
                    theSameAmount++;
                } else {
                    differenceSet.add(numCombinations.get(i));
                }
            }
        }
        differenceSet.forEach(difference -> {
            String outoutStr = difference.toString();
            System.out.println(outoutStr);
            output(outputStream, outoutStr+"\r\n");
        });
        return theSameAmount;
    }

    private static List<NumCombination> createNumCombination(List<Integer> nums) {
        List<NumCombination> numCombinations = new ArrayList<>();
        for (int i = 0; i < nums.size(); i++) {
            for (int j = i + 1; j < nums.size(); j++) {
                for (int k = j + 1; k < nums.size(); k++) {
                    NumCombination numCombination = new NumCombination(nums.get(i), nums.get(j), nums.get(k));

                    if (!numCombinations.contains(numCombination)) {
                        numCombinations.add(numCombination);
                    }
                }
            }
        }
        return numCombinations;
    }

    private static void output(FileOutputStream fileOutputStream, String str) {
        try {
            fileOutputStream.write(str.getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
